package be.jschoreels.demo.eventdriven.services.api.event;

public class ItemPriceTracked {

    String itemId;

    public ItemPriceTracked(String itemId) {
        this.itemId = itemId;
    }

    public String getItemId() {
        return itemId;
    }
}
