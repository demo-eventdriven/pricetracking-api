package be.jschoreels.demo.eventdriven.services.api.event;

public class ItemPriceUntracked {

    String itemId;

    public ItemPriceUntracked(String itemId) {
        this.itemId = itemId;
    }

    public String getItemId() {
        return itemId;
    }
}
