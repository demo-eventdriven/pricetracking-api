package be.jschoreels.demo.eventdriven.services.pricetracking.api.event;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.math.BigDecimal;

@JsonDeserialize(builder = ItemPriceIncreased.Builder.class)
public class ItemPriceIncreased {

    private final String item;
    private final BigDecimal oldPrice;
    private final BigDecimal newPrice;

    private ItemPriceIncreased(final Builder builder) {
        item = builder.item;
        oldPrice = builder.oldPrice;
        newPrice = builder.newPrice;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(final ItemPriceIncreased copy) {
        Builder builder = new Builder();
        builder.item = copy.getItem();
        builder.oldPrice = copy.getOldPrice();
        builder.newPrice = copy.getNewPrice();
        return builder;
    }

    public String getItem() {
        return item;
    }

    public BigDecimal getOldPrice() {
        return oldPrice;
    }

    public BigDecimal getNewPrice() {
        return newPrice;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonPOJOBuilder
    public static final class Builder {

        private String item;
        private BigDecimal oldPrice;
        private BigDecimal newPrice;

        private Builder() {
        }

        public Builder withItem(final String item) {
            this.item = item;
            return this;
        }

        public Builder withOldPrice(final BigDecimal oldPrice) {
            this.oldPrice = oldPrice;
            return this;
        }

        public Builder withNewPrice(final BigDecimal newPrice) {
            this.newPrice = newPrice;
            return this;
        }

        public ItemPriceIncreased build() {
            return new ItemPriceIncreased(this);
        }
    }

    @Override
    public String toString() {
        return "ItemPriceIncreased{" +
            "item='" + item + '\'' +
            ", oldPrice=" + oldPrice +
            ", newPrice=" + newPrice +
            '}';
    }
}
